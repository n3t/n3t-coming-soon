n3t Coming Soon
===============

n3t Coming Soon plugin for Joomla! enables administrators to create simple coming 
soon page, which hides entire Joomla! installation for random visitors.

Documentation
-------------

Find more at [documentation page](http://n3tcomingsoon.docs.n3t.cz/en/latest/)    