<?php
/**
 * @package n3tComingSoon
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2010 - 2015 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined( '_JEXEC' ) or die( 'Restricted access' );

JFormHelper::loadFieldClass('textarea');

class JFormFieldIPList extends JFormFieldTextarea
{

	protected $type = 'IPList';

	protected function getInput()
	{
    JHtml::_('jquery.framework');      
    $button = '<br />';
    $onclick = "jQuery('#jform_params_".$this->element['name']."').val(jQuery('#jform_params_".$this->element['name']."').val()+(jQuery('#jform_params_".$this->element['name']."').val() ? '\\n' : '')+'".$_SERVER['REMOTE_ADDR']."'); return false;";
    $button.= '<button class="btn" onclick="'.$onclick.'" href="#">'.JText::_('PLG_SYSTEM_N3TCOMINGSOON_CFG_IP_FILTER_ADD_CURRENT').'</button>';
    $onclick = "jQuery('#jform_params_".$this->element['name']."').val(''); return false;";    
    $button.= ' <button class="btn" onclick="'.$onclick.'" href="#">'.JText::_('PLG_SYSTEM_N3TCOMINGSOON_CFG_IP_FILTER_CLEAR').'</button>';
    return parent::getInput().$button;
	}
}
